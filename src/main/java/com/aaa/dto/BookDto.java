package com.aaa.dto;

/**
 * @author 杨森
 * @version 1.0
 * @Title: 书籍实体类
 * @date 2020/7/24 16:02
 */
public class BookDto {

    /**
     * 书名
     */
    private String name;

    /**
     * 书籍编码
     */
    private String bookCod;

    private String bookAuthor;

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBookCod() {
        return bookCod;
    }

    public void setBookCod(String bookCod) {
        this.bookCod = bookCod;
    }
}
