package com.aaa.dto;


import com.aaa.entity.CartoonCatalogue;

import java.util.List;

/**
 * @author 杨森
 * @version 1.0
 * @Title: cartoonCatalogue
 * @date 2020/7/28 14:56
 */
public class CartoonCatalogueDto {
    /**
     * 书名
     */
    private String cartoonName;
    /**
     * 作者
     */
    private String cartoonAuthor;

    /**
     * 书籍编码
     */
    private String cartoonCode;

    /**
     * 书籍图片
     */
    private String cartoonImage;

    /**
     * 书籍简介
     */
    private String cartoonIntro;

    private List<CartoonCatalogue> cartoonCatalogues;

    public CartoonCatalogueDto(){}

    public String getCartoonName() {
        return cartoonName;
    }

    public void setCartoonName(String cartoonName) {
        this.cartoonName = cartoonName;
    }

    public String getCartoonAuthor() {
        return cartoonAuthor;
    }

    public void setCartoonAuthor(String cartoonAuthor) {
        this.cartoonAuthor = cartoonAuthor;
    }

    public String getCartoonCode() {
        return cartoonCode;
    }

    public void setCartoonCode(String cartoonCode) {
        this.cartoonCode = cartoonCode;
    }

    public String getCartoonImage() {
        return cartoonImage;
    }

    public void setCartoonImage(String cartoonImage) {
        this.cartoonImage = cartoonImage;
    }

    public String getCartoonIntro() {
        return cartoonIntro;
    }

    public void setCartoonIntro(String cartoonIntro) {
        this.cartoonIntro = cartoonIntro;
    }

    public List<CartoonCatalogue> getCartoonCatalogues() {
        return cartoonCatalogues;
    }

    public void setCartoonCatalogues(List<CartoonCatalogue> cartoonCatalogues) {
        this.cartoonCatalogues = cartoonCatalogues;
    }
}
