package com.aaa.data;

import com.aaa.config.SSLHelper;
import com.aaa.constant.DataSource;
import com.aaa.entity.Book;
import com.aaa.util.DataProcessing;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author 杨森
 * @version 1.0
 * @Title: data source
 * @date 2020/8/4 14:49
 */
public class BookList {
    private static ExecutorService executorService;
    private static Pattern pattern = Pattern.compile("<a[^>]*href=(\\\"([^\\\"]*)\\\"|\\'([^\\']*)\\'|([^\\\\s>]*))[^>]*>(.*?)</a>");

    public static List<Book> setDataSource(String dataSource, String key, ExecutorService executorService) {
        BookList.executorService = executorService;
        try {
            Map<String, List<String>> dataSource1 = DataSource.getDataSource();
            String url = dataSource1.get(dataSource).get(0);
            String[] split = url.split("--");
            return getBookList(key, split[1], split[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<Book> getBookList(String key, String url, String way) {
        List<Book> bookDtos = null;
        try {
            Document document = null;
            if (way.equals("GET")) {
                document = Jsoup.connect(url + key).get();
            } else {
                Connection connect = Jsoup.connect(url);
                connect.data("keyword", key);
                document = connect.post();
            }
            Element element = document.select("tbody").get(0);
            Elements trs = element.getElementsByTag("tr");
            List<Element> trss = trs.stream().filter(element1 -> !element1.text().isEmpty()).collect(Collectors.toList());
            bookDtos = get1(trss, pattern);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bookDtos;
    }

    public static List<Book> get1(List<Element> tbody, Pattern pattern) {
        List<Book> bookDtos = new ArrayList<>(tbody.size());
        Map<String, Integer> map = new HashMap<>(10);
        tbody.forEach(tr -> {
            List<Node> nodes = tr.childNodes().stream().filter(node -> !node.toString().trim().isEmpty()).collect(Collectors.toList());
            Elements td = tr.getElementsByTag("td");
            String s1 = nodes.toString();
            for (int i = 0; i < nodes.size(); i++) {
                String node = nodes.get(i).toString();
                if (node.matches("(.*)文章名称(.*)") || node.matches("(.*)书名(.*)")) {
                    map.put("bookName", i);
                    continue;
                }
                if (node.matches("(.*)作者(.*)")) {
                    map.put("author", i);
                    return;
                }
            }
            Book book = new Book();
            String href = "";
            String reg = ".*</a>.*";
            if (s1.matches(reg)) {
                Matcher matcher = pattern.matcher(s1);
                if (matcher.find()) {
                    href = matcher.group(1);
                    String nameCode = href.substring(href.lastIndexOf(".com")+5,href.length()-2);
                    if("ascript:void(0)".equals(nameCode)){
                       return;
                    }
                    book.setBookCode(nameCode);
                }
            }
            book.setBookName(td.get(map.get("bookName")).text());
            book.setBookAuthor(td.get(map.get("author")).text());
            bookDtos.add(book);
        });
        return bookDtos;
    }

}
