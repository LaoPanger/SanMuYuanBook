package com.aaa.data;

import com.aaa.config.SSLHelper;
import com.aaa.dto.BookCatalogueDto;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 杨森
 * @version 1.0
 * @Title: BookChapter
 * @date 2020/8/7 15:20
 */
public class BookChapter {

    public static BookCatalogueDto setDataSource(String dataSource, String bookCod, String chapterCod) {
        SSLHelper.init();
        switch (dataSource){
            case "biquge5200" :
                return biquge5200(bookCod, chapterCod);
            case "biquge" :
                return biquge(bookCod, chapterCod);
            case "ddxs" :
                return ddxs(bookCod, chapterCod);
        }
        return null;
    }

    private static BookCatalogueDto ddxs(String bookCod, String chapterCod) {
        try {
            BookCatalogueDto bookCatalogueDto=new BookCatalogueDto();
            Document document = Jsoup.connect("http://www.ddxs.com/" + bookCod + "/" + chapterCod + ".html").get();
            Element amain = document.getElementById("amain");
            Elements chapterName =amain.getElementsByTag("h1");
            bookCatalogueDto.setCatalogueName(chapterName.text());
            String p = document.select("#contents").html();
            String replace = p.replace("<script>show_htm2();</script> ", "")
                    .replace("/n", "")
                    .replace("<script>show_htm3();</script>", "")
                    .replace("<p>", "<p data-type=\"2\"><span class=\"content-wrap\">")
                    .replace("</p>", "</span></p>");
            bookCatalogueDto.setCatalogueText(replace);
            Element footlink = document.getElementById("footlink");
            Elements a = footlink.getElementsByTag("a");
            String href = a.get(0).attr("href").replace("/", "").replace(bookCod, "").replace(".html", "");
            if(!"".equals(href)){
                href= href.replace(".html", "");
                bookCatalogueDto.setUpCode(Integer.parseInt(href));
            }else{
                bookCatalogueDto.setUpCode(null);
            }
            href = a.get(2).attr("href").replace("/"+bookCod+"/", "");
            if(!"".equals(href)){
                href= href.replace(".html", "");
                bookCatalogueDto.setNextCode(Integer.parseInt(href));
            }else{
                bookCatalogueDto.setNextCode(null);
            }
            bookCatalogueDto.setCatalogueCod(Integer.parseInt(chapterCod));
            bookCatalogueDto.setBookCod(bookCod);
            return bookCatalogueDto;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static BookCatalogueDto biquge5200(String bookCod, String chapterCod) {
        BookCatalogueDto bookCatalogueDto = new BookCatalogueDto();
        Document document = null;
        try {
            document = Jsoup.connect("https://www.biquge5200.com/" + bookCod + "/" + chapterCod + ".html").get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //<a href="https://www.biquge5200.com/77_77704/148480592.html">下一章</a>
        //获得的章节名称
        Elements chapterName = document.select("h1");
        bookCatalogueDto.setCatalogueName(chapterName.text());
        String p = document.select("#content").html();
        Elements elementsByClass = document.getElementsByClass("bottem1");
        //获取下一章
        String node = elementsByClass.get(0).childNode(9).toString();
        Pattern pattern = Pattern.compile("<a\\s*href=\"?([\\w\\W]*?)\"?[\\s]*?[^>]>([\\s\\S]*?)(?=</a>)");
        Matcher matcher = pattern.matcher(node);
        if (matcher.find()) {
            String nameCodeUrl = matcher.group(1);
            if(!("https://www.biquge5200.com/"+bookCod+"/").equals(nameCodeUrl)) {
                String insStr = nameCodeUrl.substring(nameCodeUrl.lastIndexOf("/") + 1, nameCodeUrl.lastIndexOf("."));
                bookCatalogueDto.setNextCode(Integer.parseInt(insStr));
            }
        }
        //获取上一章
        String node1 = elementsByClass.get(0).childNode(5).toString();
        Matcher matcher1 = pattern.matcher(node1);
        if (matcher1.find()) {
            String nameCodeUrl = matcher1.group(1);
            if(!("https://www.biquge5200.com/"+bookCod+"/").equals(nameCodeUrl)){
                String insStr = nameCodeUrl.substring(nameCodeUrl.lastIndexOf("/") + 1, nameCodeUrl.lastIndexOf("."));
                bookCatalogueDto.setUpCode(Integer.parseInt(insStr));
            }
        }
        String str = p.replace("<div id='content' style='width: 85%;'>", "")
                .replace("/n", "")
                .replace("</div>", "")
                .replace("<p>", "<p data-type=\"2\"><span class=\"content-wrap\">")
                .replace("</p>","</span></p>");
        bookCatalogueDto.setCatalogueText(str);
        bookCatalogueDto.setCatalogueCod(Integer.parseInt(chapterCod));
        bookCatalogueDto.setBookCod(bookCod);

        return bookCatalogueDto;
    }

    private static BookCatalogueDto biquge(String bookCod, String chapterCod) {
        BookCatalogueDto bookCatalogue = new BookCatalogueDto();
        Document document = null;
        try {
            document = Jsoup.connect("https://www.biquge.com/" + bookCod + "/" + chapterCod + ".html").get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String p = document.select("#content").html().replace("<script>readx();</script>", "").replace("<script>chaptererror();</script>", "");
        bookCatalogue.setCatalogueText(p);
        bookCatalogue.setCatalogueCod(Integer.parseInt(chapterCod));
        Elements chapterName = document.select("h1");
        bookCatalogue.setCatalogueName(chapterName.text());
        Elements next = document.getElementsByClass("next");
        String nextHtml = next.get(0).html();
        Pattern pattern = Pattern.compile("<a\\s*href=\"?([\\w\\W]*?)\"?[\\s]*?[^>]>([\\s\\S]*?)(?=</a>)");
        Matcher matcher = pattern.matcher(nextHtml);
        if (matcher.find()) {
            String nameCodeUrl = matcher.group(1);
            String insStr = nameCodeUrl.substring(nameCodeUrl.lastIndexOf("/") + 1, nameCodeUrl.lastIndexOf("."));
            bookCatalogue.setNextCode(Integer.parseInt(insStr));
        }
        return bookCatalogue;
    }


}
