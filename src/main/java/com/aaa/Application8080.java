package com.aaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 杨森
 * @version 1.0
 * @Title: 启动类
 * @date 2020/7/24 15:50
 */
@SpringBootApplication
public class Application8080 {
    public static void main(String[] args) {
        SpringApplication.run(Application8080.class,args);
    }
}
