(function ($) {
    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }
})(jQuery);
$(function () {
    $.ajax({
        type: "POST",
        url: "/cartoon/getCartoonCatalogue",
        data: {
            "cartoonCod": $.getUrlParam('cartoonCod'),
            "dataSource": $.getUrlParam('dataSource')
        },
        dataType: "json",
        success: function (data) {
            $("#bookName").html(data.cartoonName);
            let strhtml = "";
            let size=0;
            for (let i = 0; i < data.cartoonCatalogues.length; i++) {
                strhtml+= "<li data-rid=\""+(i+1)+"\"><a href='readCartoon.html?cartoonCod=" + data.cartoonCode
                    + "&&chapterCod="+data.cartoonCatalogues[i].catalogueCode+"&&dataSource="+$.getUrlParam('dataSource')+"'>" + data.cartoonCatalogues[i].catalogueName + "</a></li>";
            }
            $("#bookImg").append("<img src='"+data.cartoonImage+"'/>");
            $("#author").append(data.cartoonAuthor);
            $("#intro").append(data.cartoonIntro);
            $("#body").append(strhtml);
        },
        complete: function (data) {
        }
    });
});
function show() {
    alert("由于资金不足,本网站不配备数据库,一切数据现爬现下,故下载速度较慢,请耐心等待");
    window.location="/cartoon/downloadCartoon?cartoonCod="+$.getUrlParam('cartoonCod')+"&&bookName1="+$("#bookName").text()+"&&dataSource="+$.getUrlParam('dataSource');
}

