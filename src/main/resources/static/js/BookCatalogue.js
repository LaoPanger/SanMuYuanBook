(function ($) {
    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }
})(jQuery);
$(function () {
    $.ajax({
        type: "POST",
        url: "/book/getBookCatalogue",
        data: {
            "bookCod": $.getUrlParam('bookCod'),
            "dataSource": $.getUrlParam('dataSource')
        },
        dataType: "json",
        success: function (data) {
            $("#bookName").html(data[0].bookName);
            let strhtml = "";
            let size=0;
            for (let i = 0; i < data.length; i++) {
                strhtml+= "<li data-rid=\""+(i+1)+"\"><a href='readBook.html?bookCod=" + data[i].bookCode + "&&chapterCod="+data[i].catalogueCod+"&&dataSource="+$.getUrlParam('dataSource')+"'>" + data[i].catalogueName + "</a></li>";
            }

            $("#bookImg").append(data[0].bookImage);
            $("#author").append(data[0].bookAuthor);
            $("#intro").append(data[0].bookIntro);
            $("#body").append(strhtml);
        },
        complete: function (data) {
        }
    });
});
function show() {
    window.location="/book/downloadBook?bookCod="+$.getUrlParam('bookCod')+"&&bookName1="+$("#bookName").text()+"&&dataSource="+$.getUrlParam('dataSource');
}

