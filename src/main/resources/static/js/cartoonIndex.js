function show() {
    $('#list').empty();
    $.ajax({
        type:"POST",
        url:"cartoon/pageList",
        data:{
            "key":$("#s-box").val(),
            "dataSource":$("#dataSource").val()
        },
        success:function (data) {
            if(data.length===0){
                $("#list").append("<p>。。。。。。。没找到这本书哟，换换数据源再找找叭！</p>");
                return
            }
            var strhtml="";
            for (let i = 0; i < data.length; i++) {
                strhtml+="<li class=\"res-book-item\" data-bid=\"1019664125\" data-rid=\"1\">\n" +
                    "                            <div class=\"book-img-box\">\n" +
                    "                                <a href=\"page/CartoonCatalogue.html?cartoonCod="+data[i].cCode+"&&dataSource="+$("#dataSource").val()+"\" target=\"_blank\" data-eid=\"qd_S04\" data-algrid=\"0.0.0\" data-bid=\"1019664125\"><img src=\""+data[i].cImgSrc+"\"></a>\n" +
                    "                            </div>\n" +
                    "                            <div class=\"book-mid-info\">\n" +
                    "                                \n" +
                    "                                <h4><a href=\"page/CartoonCatalogue.html?cartoonCod="+data[i].cCode+"&&dataSource="+$("#dataSource").val()+"\" target=\"_blank\" data-eid=\"qd_S05\" data-bid=\"1019664125\" data-algrid=\"0.0.0\"><cite class=\"red-kw\">"+data[i].cName+"</cite></a></h4>\n" +
                    "                                <p class=\"author\">\n" +
                    "                                    <img src=\"img/user.f22d3.png\"><a class=\"name\" data-eid=\"qd_S06\" href=\"page/CartoonCatalogue.html?cartoonCod="+data[i].cCode+"&&dataSource="+$("#dataSource").val()+"\" target=\"_blank\">"+data[i].cAuthor+"</a> <em>|</em>\n" +
                    "                                </p>\n" +
                    "                                <p class=\"intro\">\n" +
                    "                                   暂无简介\n" +
                    "                                </p>\n" +
                    "                                <p class=\"update\"><a href=\"page/CartoonCatalogue.html?cartoonCod="+data[i].cCode+"&&dataSource="+$("#dataSource").val()+"\" target=\"_blank\" data-eid=\"qd_S08\" data-bid=\"1019664125\">——————————————————————————</a><em>·</em><span>15小时前</span>\n" +
                    "                                </p>\n" +
                    "                            </div>\n" +
                    "                            <div class=\"book-right-info\">\n" +
                    "                                <p class=\"btn\">\n" +
                    "                                    <a class=\"red-btn\" href=\"page/CartoonCatalogue.html?cartoonCod="+data[i].cCode+"&&dataSource="+$("#dataSource").val()+"\" data-eid=\"qd_S02\" target=\"_blank\">书籍详情</a>\n" +
                    "                                    \n" +
                    "                                </p>\n" +
                    "                            </div>\n" +
                    "                        </li>";
            }
            $("#list").append(strhtml);
        }
    })

}