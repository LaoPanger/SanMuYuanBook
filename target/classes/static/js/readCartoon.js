(function ($) {
    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }
})(jQuery);
$(function () {
    $.ajax({
        type: "POST",
        url: "/cartoon/getCartoonChapter",
        data: {
            "cartoonCod": $.getUrlParam('cartoonCod'),
            "chapterCod": $.getUrlParam('chapterCod')
        },
        dataType: "json",
        success: function (data) {
            for (let i = 0; i < data.catalogueSrc.length; i++) {
                $("#body").append("<img class='imgs' src='"+data.catalogueSrc[i]+"'/>");
            }
            $("#head").append(data.catalogueName);
            if(data.upCode!=null){
                 $("#j_chapterPrev").attr("href","readCartoon.html?cartoonCod="+$.getUrlParam('cartoonCod')+"&&chapterCod="+data.upCode);
            }
            $(".back").attr("href","CartoonCatalogue.html?cartoonCod="+$.getUrlParam('cartoonCod'))
            if(data.nextCode!=null){
                $("#j_chapterNext").attr("href","readCartoon.html?cartoonCod="+$.getUrlParam('cartoonCod')+"&&chapterCod="+data.nextCode);
            }
        },
        complete: function (data) {

        }
    });
})